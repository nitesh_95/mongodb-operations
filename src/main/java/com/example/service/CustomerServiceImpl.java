package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.dao.CustomerRepository;
import com.example.model.Customer;

public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public void saveOrUpdateCustomer(Customer customer) {
		customerRepository.save(customer);

	}

	@Override
	public void deleteCustomer(String id) {
		customerRepository.deleteById(id);

	}

}
