package com.example.service;

import java.util.List;

import com.example.model.Customer;

public interface CustomerService {
	List<Customer> findAll();

	void saveOrUpdateCustomer(Customer customer);

	void deleteCustomer(String id);
}
