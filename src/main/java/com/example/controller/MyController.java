package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Customer;
import com.example.service.CustomerService;

@RestController
public class MyController {
	@Autowired
	private CustomerService customerService;

	@GetMapping(path = "/getCustomer")
	public List<Customer> getAllCustomer() {
		return customerService.findAll();
	}

	@PostMapping(value = "/save")
	public ResponseEntity<?> saveOrUpdateStudent(@RequestBody Customer customer) {
		customerService.saveOrUpdateCustomer(customer);
		return new ResponseEntity("Customer added successfully", HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete/{custId}")
	    public void deleteStudent(@PathVariable("custId") String custId) {
	    	customerService.deleteCustomer(custId);
	}
}
